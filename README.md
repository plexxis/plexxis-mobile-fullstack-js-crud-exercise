# Plexxis Interview Exercise
## Requirements
Create a simple but __impressive__ (looks good, works well, has intuitive design, etc.) CRUD application that can do the following:

1) Retrieve employees from a REST API  
2) Display the employees in a React application  
3) Has UI mechanisms for creating and deleting employees  
4) Has API endpoints for creating and deleting employees  
5) Edit your version of the `README.md` file to explain to us what things you did, where you focussed your effort, etc.

*Read over the `Bonus` objectives and consider tackling those items as well*

## Bonus (Highly Encouraged)

1) Use a relational database to store the data (SQLite, MariaDB, Postgres)  
2) UI mechanisms to edit/update employee data  
3) Add API endpoint to update employee data  
4) Use [React Table](https://react-table.js.org), if you choose React as your frontend. 

## Getting Started
The frontend was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). 

* To run frontend, use command line and run "npm start". 
* You have option to use any other framework, such as Angular, Vue or Knockout. 

The backend was scaffolded with ASP.NET Core. 

* To run server, open /backend/Plexxis Mobile FullStack JS CRUD Exercise.sln and run the app. 
* You can choose different web framework for example ASP.NET, and build server on your own. 

## Getting it Done
* You are free to use whatever libraries that you want. Be prepared to defend your decisions.
* There is no time limit. Use as little or as much time as is necessary to showcase your abilities.
* You should fork or clone our repository into your own repository.
  * Send us the link when you are done the exercise (pglinker at plexxis dot com).

If you do well on the test, we will bring you in for an interview. Your test results will be used as talking points.  

 __This is your chance to amaze us with your talent!__
